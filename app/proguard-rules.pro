# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile
-optimizationpasses 5
-dontpreverify
-dontoptimize
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-verbose
-dontobfuscate
-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*

# As described in tools/proguard/examples/android.pro - ignore all warnings.
-dontwarn android.support.v4.**
-dontwarn android.support.v7.**
-dontwarn javax.mail.**
-dontwarn javax.naming.Context
-dontwarn javax.naming.InitialContext
-dontwarn android.app.Application**
-dontwarn java.lang.String
-dontwarn com.google.android.gms.**

-dontwarn java.beans.**
-dontwarn org.apache.tools.**
-dontwarn org.junit.**
-dontwarn sun.reflect.**
-dontwarn org.mockito.**

#---Lib dependancy error fix in 1.1.7--
-keep public class android.net.http.SslError
-keep public class android.webkit.WebViewClient

-dontwarn android.webkit.WebView
-dontwarn android.net.http.SslError
-dontwarn android.webkit.WebViewClient
-dontwarn org.apache.http.conn.scheme.LayeredSocketFactory
-dontwarn org.apache.http.conn.ssl.SSLSocketFactory
-dontwarn org.apache.http.params.HttpConnectionParams
#-- End of fix--

# Keep classes for these categories
-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.app.backup.BackupAgentHelper
-keep public class * extends android.preference.Preference
-keep public class * extends android.view.View
-keep public class * extends android.app.Fragment
-keep public class * extends android.support.v4.app.Fragment

-keep public class tv.accedo.wynk.android.airtel.data.player.helper.VideoplazaPlugin { *; }
-keep public class tv.accedo.wynk.android.airtel.model.appgrid.SearchSection { *; }
-keep public class tv.accedo.wynk.android.airtel.model.appgrid.AboutSection { *; }
-keep public class tv.accedo.wynk.android.airtel.model.Questions { *; }
-keep public class tv.accedo.wynk.android.airtel.model.Section { *; }
-keep public class tv.accedo.wynk.android.airtel.model.Bitrates { *; }
-keep public class tv.accedo.wynk.android.airtel.model.** { *; }
-keep public class tv.accedo.wynk.android.airtel.model.appgrid.** { *; }
-keep public class tv.accedo.wynk.android.airtel.data.** { *; }
-keep public class tv.accedo.wynk.android.airtel.data.analytics.** { *; }
-keep public class tv.accedo.wynk.android.airtel.data.helper.** { *; }
-keep public class tv.accedo.wynk.android.airtel.data.manager.** { *; }
-keep public class tv.accedo.wynk.android.airtel.data.player.** { *; }
-keep public class tv.accedo.wynk.android.airtel.data.provider.** { *; }
-keep public class tv.accedo.wynk.android.airtel.util.** { *; }
-keep public class tv.accedo.wynk.android.airtel.util.constants.** { *; }
-keep public class tv.accedo.wynk.android.airtel.util.exceptions.** { *; }
-keepnames class * implements java.io.Serializable

-keep interface tv.accedo.wynk.android.blocks.service.** { *; }
-keep interface tv.accedo.wynk.android.airtel.interfaces.** { *; }

-keep public class tv.accedo.wynk.android.blocks.model.** { *; }
-keep public class wynk.domain.model.** { *; }
-keep public class wynk.data.entity.** { *; }
-keep public class tv.accedo.wynk.android.airtel.livetv.model.** { *; }
-keep public class tv.accedo.wynk.android.airtel.livetv.v2.models.** { *; }
-keep public class wynk.presentation.modules.hotstar.models.** { *; }

-keep class android.support.v7.*
-keep class se.videoplaza.kit.** { *; }

#---BufferdSource and MapActivity
-keep class okio.BufferedSource
-keep class okio.Buffer
-keep class android.content.Context
-keep class com.comscore.instrumentation.InstrumentedMapActivity.** {*;}

-dontwarn okio.BufferedSource
-dontwarn okio.Buffer
-dontwarn android.content.Context
-dontwarn com.comscore.instrumentation.InstrumentedMapActivity

#-----Retrofit and OkHttp-------------
-keepattributes *Annotation*
-keepattributes Signature
-keep class com.squareup.okhttp.** { *; }
-keep interface com.squareup.okhttp.** { *; }
-dontwarn com.squareup.okhttp.**
-dontwarn rx.**
-dontwarn retrofit.**
-keep class retrofit.** { *; }
-keepclasseswithmembers class * {
    @retrofit.http.* <methods>;
}
-dontwarn java.nio.file.*
-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement
#--------------------------------------------

#----------New classes to keep -----------
-keep class org.robolectric.shadows.** { *; }
-keep class org.hamcrest.** { *; }
-keep class com.quickplay.** { *; }
-keep class edu.emory.mathcs.backport.java.util.concurrent.helpers.** { *; }
-keep class com.insidesecure.drmagent.** { *; }
-keep class com.crashlytics.android.** { *; }
#-keep class com.newrelic.agent.android.** { *; }
-keep class android.app.notification.** { *; }
-keep class android.graphics.** { *; }
-keep public class tv.accedo.wynk.android.activity.** { *; }
-keep class tv.accedo.wynk.android.airtel.fragment.NavigationFragment
-keep class tv.accedo.wynk.android.airtel.manager.vstb.services.**
-keep class com.nexstreaming.nexplayerengine.** { *; }
-keep class tv.accedo.wynk.android.airtel.playerdata.NexPlayer.** { *; }
-keep class android.support.multidex.** { *; }

# ---- MoEngage---
-dontwarn com.google.android.gms.location.**
-dontwarn com.google.android.gms.gcm.**
-dontwarn com.google.android.gms.iid.**

-keep class com.google.android.gms.gcm.** { *; }
-keep class com.google.android.gms.iid.** { *; }
-keep class com.google.android.gms.location.** { *; }

-keep class com.moe.pushlibrary.activities.** { *; }
-keep class com.moengage.locationlibrary.GeofenceIntentService
-keep class com.moe.pushlibrary.InstallReceiver
-keep class com.moengage.push.MoEPushWorker
-keep class com.moe.pushlibrary.providers.MoEProvider
-keep class com.moengage.receiver.MoEInstanceIDListener
-keep class com.moengage.worker.MoEGCMListenerService
-keep class com.moe.pushlibrary.models.** { *;}
-keep class com.moengage.core.GeoTask
-keep class com.moengage.location.GeoManager
-keep class com.moengage.inapp.InAppManager
-keep class com.moengage.push.PushManager
-keep class com.moengage.inapp.InAppController

-keep class com.moengage.pushbase.activities.PushTracker
-keep class com.moengage.pushbase.activities.SnoozeTracker
-keep class com.moengage.pushbase.push.MoEPushWorker
-keep class com.moe.pushlibrary.MoEWorker
-keep class com.moe.pushlibrary.AppUpdateReceiver
-keep class com.moengage.core.MoEAlarmReceiver


-dontwarn com.moengage.location.GeoManager
-dontwarn com.moengage.core.GeoTask
-dontwarn com.moengage.receiver.*
-dontwarn com.moengage.worker.*
-dontwarn com.moengage.inapp.ViewEngine

-keep class com.delight.**  { *; }

-keep class com.segment.analytics.** { *; }
-keep class com.segment.analytics.Analytics { *; }
-keep class com.segment.analytics.Analytics$Builder { *; }


-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
    **[] $VALUES;
    public *;
}

-dontwarn org.robolectric.shadows.**
-dontwarn org.hamcrest.**
-dontwarn com.quickplay.**
-dontwarn edu.emory.mathcs.backport.java.util.concurrent.helpers.**
-dontwarn com.insidesecure.drmagent.**
-dontwarn com.quickplay.test.**
-dontwarn com.crashlytics.android.**
#-dontwarn com.newrelic.agent.android.**
-dontwarn tv.accedo.wynk.android.activity.**
-dontwarn wynk.presentation.**
-dontwarn tv.accedo.wynk.android.airtel.adapter.**
-dontwarn kotlin.reflect.jvm.internal.impl.builtins.**
-dontwarn tv.accedo.wynk.android.airtel.manager.vstb.services.**
-dontwarn com.nexstreaming.nexplayerengine.**
-dontwarn tv.accedo.wynk.android.airtel.playerdata.NexPlayer.**
-dontwarn android.support.multidex.**
-dontwarn com.moe.**
-dontwarn com.moengage.**
-dontwarn com.segment.analytics.**

#---------------------------------------

-keep public class org.apache.** {
  <fields>;
  <methods>;
}

-keep class tv.accedo.wynk.android.activity.** {
  <fields>;
  <methods>;
}

-keep public class com.crashlytics.android.** {
    <fields>;
    <methods>;
    }

-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}

-keepclassmembers class * extends android.app.Activity {
   public void *(android.view.View);
}

#-ignorewarnings

#----------------------------------------------------
-keep public class com.google.android.gms.* { public *; }

-keepclasseswithmembernames class * {
    native <methods>;
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context);
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet);
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}

-keepclassmembers class * {
    public *;
}

-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

# The maps library uses custom Parcelables.  Use this rule (which is slightly
# broader than the standard recommended one) to avoid obfuscating them.
-keepclassmembers class * implements android.os.Parcelable {
    static *** CREATOR;
}

# Missing annotations are harmless.
-dontwarn sun.misc.Unsafe
-dontwarn javax.annotation.**

######## Opciones para ACRA:
# we need line numbers in our stack traces otherwise they are pretty useless
-renamesourcefileattribute SourceFile
-keepattributes Exceptions,InnerClasses,Signature,Deprecated,SourceFile,LineNumberTable,*Annotation*,EnclosingMethod

# ACRA needs "annotations" so add this...
-keepattributes *Annotation*

# Skipping Classes for Next Streaming Player
-dontwarn com.conviva.streamerProxies.**
-dontwarn tv.freewheel.renderers.**

 # Appcompat Material Design library
 -keep class android.support.v7.** { *; }
 -keep interface android.support.v7.** { *; }

 # support design library
 -dontwarn android.support.design.**
 -keep class android.support.design.** { *; }
 -keep interface android.support.design.** { *; }
 -keep public class android.support.design.R$* { *; }

 -keep class com.squareup.wire.** { *; }
 -keep class tv.accedo.wynk.android.airtel.analytics.model.** { *; }
 -keep class wynk.data.repository.datasource.impl.** { *; }
 -keep class tv.** { *; }

 -printmapping outputfile.txt

-assumenosideeffects class android.util.Log {
    public static int d(...);
    public static int v(...);
    public static int i(...);
    public static int w(...);
    public static int e(...);
    public static int wtf(...);
}

-dontnote retrofit2.Platform
# Platform used when running on Java 8 VMs. Will not be used at runtime.
-dontwarn retrofit2.Platform$Java8
# Retain generic type information for use by reflection by converters and adapters.
-keepattributes Signature
# Retain declared checked exceptions for use by a Proxy instance.
-keepattributes Exceptions
-dontwarn okio.**
-dontwarn okhttp3.**
-dontwarn java.lang.invoke**
-dontwarn com.google.sample.castcompanionlibrary.**

-dontnote android.net.http.*
-dontnote org.apache.http.**
-dontwarn com.google.auto.value.AutoValue
-dontwarn org.checkerframework.**
-dontwarn java.lang.ClassValue
-dontwarn afu.org.checkerframework.**
-dontwarn javax.lang.model.element.Modifier
-dontwarn com.google.auto.factory.AutoFactory
-dontwarn com.google.auto.factory.Provided

#inmobi
-keepattributes SourceFile,LineNumberTable
-keep class com.inmobi.** { *; }
-keep public class com.google.android.gms.**
-dontwarn com.google.android.gms.**
-dontwarn com.squareup.picasso.**
-keep class com.google.android.gms.ads.identifier.AdvertisingIdClient{
     public *;
}
-keep class com.google.android.gms.ads.identifier.AdvertisingIdClient$Info{
     public *;
}
# skip the Picasso library classes
-keep class com.squareup.picasso.** {*;}
-dontwarn com.squareup.picasso.**
-dontwarn com.squareup.okhttp.**
# skip Moat classes
-keep class com.moat.** {*;}
-dontwarn com.moat.**
# For old ads classes
-keep public class com.google.ads.**{
   public *;
}
# For mediation
-keepattributes *Annotation*
# For Google Play services
-keep public class com.google.android.gms.ads.**{
   public *;
}


# proguard configuration for Gson
-keepattributes Signature
-keep public class com.google.gson
-keep class sun.misc.Unsafe { *; }

-keep class com.comcasystems.routedriver.jsonclasses.** { *; }
