package com.atv.demo;
import android.os.Bundle;
import android.util.Log;

import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;

import com.atv.demo.ui.main.SectionsPagerAdapter;

import org.json.JSONObject;

import tv.africa.AtvSdkSupport;
import tv.africa.wynk.android.airtel.WynkApplication;
import tv.africa.wynk.android.airtel.activity.base.BaseActivity;

public class MainActivity extends BaseActivity  {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setOffscreenPageLimit(3);
        viewPager.setAdapter(sectionsPagerAdapter);

        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);


        AtvSdkSupport.getAtvSdk().getTvContent(WynkApplication.getContext(), new AtvSdkSupport.OnContentReceived() {
            @Override
            public void onNext(JSONObject s) {
                Log.d("MainActivityContent",s.toString());
            }

            @Override
            public void onFailed(String s) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

}